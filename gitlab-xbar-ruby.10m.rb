#!/usr/bin/env ruby

# Metadata allows your plugin to show up in the app, and website.
#
# <xbar.title>GitLab</xbar.title>
# <xbar.version>v0.0.1</xbar.version>
# <xbar.author>Alex Buijs</xbar.author>
# <xbar.author.gitlab>alexbuijs</xbar.author.gitlab>
# <xbar.desc>GitLab menu item</xbar.desc>
# <xbar.dependencies>ruby</xbar.dependencies>
# <xbar.var>string(GITLAB_URL="https://gitlab.com"): GitLab base URL.</xbar.var>
# <xbar.var>string(GITLAB_PAGE_SIZE="10"): GitLab `per_page` size (Must be between 1-100).</xbar.var>
# <xbar.var>string(GITLAB_TOKEN=""): GitLab API access token (read_api scope).</xbar.var>

require_relative 'app/app'

App.setup

puts App.render [
  MenuBar,
  Assignee,
  Reviewer,
  Issues,
  Todos,
  Version,
  Refresh
]
