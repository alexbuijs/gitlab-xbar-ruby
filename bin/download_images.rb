#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'base64'
require 'json'
require 'rexml'

ROOT_DIR = File.expand_path('..', __dir__)

IMAGE_MAP = File.join(ROOT_DIR, 'assets/image_locations.json')
PROCESSED_IMAGES = File.join(ROOT_DIR, 'assets/images.json')

SVGImage = Struct.new(:local, :remote) do
  def download
    if content != nil

      File.write(local, content)
      puts "#{local}: downloaded"
    else
      puts "#{local}: not found(#{response.code}) on #{remote}"
    end
  end

  def content
    return @content if defined?(@content)

    @content = URI.parse(remote)
      .then { |uri| Net::HTTP.get_response(uri) }
      .then do |response|
        if response.code.to_i == 200
          ensure_size(response.body)
        end
      end
  end

  def jq_name
    @jq_name ||= File.basename(local, '.svg').gsub(/-/, '_').downcase
  end

  def base64_content
    @base64_content ||= Base64.strict_encode64(content)
  end

  private

  def ensure_size(svg)
    REXML::Document
      .new(svg)
      .tap { |doc| doc.root.add_attributes("width" => 16, "height" => 16) }
      .to_s
  end
end

JSON.parse(File.read(IMAGE_MAP), symbolize_names: true).each_with_object({}) do |data, list|
  image = SVGImage.new(File.join(ROOT_DIR, data[:local]), data[:remote])

  image.download

  list[image.jq_name] = image.base64_content
end.then do |images|
  File.write(PROCESSED_IMAGES, JSON.pretty_generate(images))
end
