class ApiRequest
  def initialize(query, variables = {})
    @api_endpoint = URI(Utils.gitlab_url('api/graphql'))
    @http = Net::HTTP.new(@api_endpoint.host, @api_endpoint.port)
    @http.use_ssl = true if @api_endpoint.instance_of?(URI::HTTPS)
    @query = load_query(query)
    @variables = variables.merge(limit: App::GITLAB_PAGE_SIZE)
  end

  def execute
    response = @http.post(@api_endpoint.path, params.to_json, headers)
    return response.body if response.is_a?(Net::HTTPSuccess)

    raise StandardError.new(response.value)
  end

  private

  def load_query(query)
    Utils.read_file("graphql/#{query}.graphql")
  end

  def headers
    {
      'PRIVATE-TOKEN' => App::GITLAB_TOKEN,
      'Content-Type' => 'application/json'
    }
  end

  def params
    {
      query: @query,
      variables: @variables
    }
  end
end
