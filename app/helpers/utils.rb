class Utils
  class << self
    def clean(string)
      string.gsub('|', '-').gsub(/\s+/, ' ')
    end

    def gitlab_url(url)
      "#{App::GITLAB_URL}/#{url}"
    end

    def read_file(file_name)
      dir = File.dirname(File.realpath(__FILE__))
      file = File.join(dir, '..', '..', file_name)
      File.read(file)
    end

    def display_time(time)
      Time.iso8601(time).getlocal.strftime('%d/%m %H:%M')
    end
  end
end
