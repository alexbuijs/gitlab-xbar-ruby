class MenuBar < MenuItem
  def title
    "⬆ #{assigned_count} ⬇ #{request_count}"
  end

  def render_title
    errors ? ':warning:' : title
  end

  def render_disabled; end

  def attrs
    { image: App.images['gitlab'] }
  end

  private

  def assigned_count
    data[:assigned_count]
  end

  def request_count
    data[:request_count]
  end
end
