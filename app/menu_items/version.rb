class Version < MenuItem
  def image
    :deployment
  end

  def title
    data['version']
  end

  def link
    'https://about.gitlab.com/releases/'
  end
end
