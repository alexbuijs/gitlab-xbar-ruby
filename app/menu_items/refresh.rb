class Refresh < MenuItem
  def image
    :retry
  end

  def title
    'refresh data'
  end

  def attrs
    { refresh: true }
  end
end
