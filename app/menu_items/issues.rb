class Issues < MenuItem
  def image
    :issue
  end

  def link
    Utils.gitlab_url("dashboard/issues?assignee_username=#{App.username}")
  end

  def submenu_item_title(item)
    "[##{item['iid']}] " + item['title']
  end
end
