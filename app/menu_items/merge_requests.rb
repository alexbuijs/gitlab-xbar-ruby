class MergeRequests < MenuItem
  def image
    :merge_request
  end

  def link
    Utils.gitlab_url("dashboard/merge_requests?#{type}_username=#{App.username}")
  end

  def submenu_item_title(item)
    "[!#{item['iid']}] " + approval_state(item) + author(item) + item['title']
  end

  def submenu_item_image(item)
    pipeline_status = item.dig('headPipeline', 'status')
    "pipeline_#{pipeline_status}".downcase.to_sym
  end

  private

  def author(item)
    author_name = item.dig('author', 'name')
    author_name ? author_name + ' - ' : ''
  end

  def approval_state(item)
    approvalsLeft = item['approvalsLeft']
    approvalsRequired = item['approvalsRequired']
    return '' unless approvalsLeft && approvalsRequired

    " (#{approvalsRequired - approvalsLeft}/#{approvalsRequired}) "
  end
end
