class Todos < MenuItem
  def image
    :todo
  end

  def link
    Utils.gitlab_url('dashboard/todos')
  end

  def submenu_item_title(item)
    [
      submenu_item_reason(item),
      submenu_item_time(item),
      submenu_item_author(item),
      item['title']
  ].join(' ')
  end

  def submenu_item_link(item)
    item.dig('note', 'url') || item.dig('targetEntity', 'url')
  end

  def submenu_item_image(item)
    item_type = item.dig('targetEntity', 'type')
    item_type.gsub(/([a-z])([A-Z])/,'\1_\2').downcase.to_sym
  end

  private

  def submenu_item_reason(item)
    case item['reason']
      when 'approval_required' then '📝'
      when 'assigned' then '☎️'
      when 'build_failed' then '💣'
      when 'directly_addressed' then '☎️'
      when 'marked' then '✅'
      when 'member_access_requested' then '📝'
      when 'mentioned' then '☎️'
      when 'merge_train_removed' then '💣'
      when 'okr_checkin_requested' then '📝'
      when 'review_requested' then '🕵️'
      when 'review_submitted' then '🕵️'
      when 'unmergeable' then '💣'
      else item['reason']
    end
  end

  def submenu_item_time(item)
    Utils.display_time(item['createdAt'])
  end

  def submenu_item_author(item)
    "[#{item.dig('author', 'name')}]"
  end
end
