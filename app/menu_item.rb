class MenuItem
  attr_accessor :title, :image, :link, :attrs
  attr_reader :data, :items

  def self.render
    new.render
  end

  def initialize
    @data = App.respond_to?(type) ? App.public_send(type) : {}
    @items = data['items'] || []
  end

  def render
    rendered = [
      Utils.clean(render_title),
      render_image,
      render_link,
      render_attrs,
      render_disabled
    ].flatten.compact.join(' | ')
    return rendered unless item_count > 0

    rendered << App.render(submenu_items, top_level: false)
  end

  private

  def render_title
    errors ? ":warning: #{errors}" : title
  end

  def render_image
    return if !image || errors

    image_data = App.images[image.to_s]
    to_attrs(templateImage: image_data) if image_data
  end

  def render_link
    to_attrs(href: link) if link && !errors
  end

  def render_attrs
    to_attrs(attrs) if attrs
  end

  def render_disabled
    to_attrs(disabled: true) if errors
  end

  def item_count
    items.length
  end

  def submenu_items
    items.map do |item|
      MenuItem.new.tap do |menu_item|
        menu_item.title = submenu_item_title(item)
        menu_item.image = submenu_item_image(item)
        menu_item.link = submenu_item_link(item)
        menu_item.attrs = { length: 120 }
      end
    end
  end

  def title
    @title || "#{type}#{counter}"
  end

  def submenu_item_link(item)
    item['url']
  end

  def submenu_item_image(_); end

  def has_next_page?
    data.dig('pageInfo', 'hasNextPage')
  end

  def type
    self.class.name.downcase.to_sym
  end

  def counter
    " (#{item_count}#{'+' if has_next_page?})" if item_count > 0
  end

  def errors
    data[:errors]
  end

  def to_attrs(hash)
    hash.map { |key, value| "#{key}=#{value}" }
  end
end
