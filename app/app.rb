require 'net/http'
require 'json'
require 'time'

require_relative 'menu_item'
require_relative 'menu_items/merge_requests'
Dir.glob(File.join(__dir__, '*', '**', '*.rb'), &method(:require))

class App
  GITLAB_URL = ENV['GITLAB_URL']
  GITLAB_TOKEN = ENV['GITLAB_TOKEN']
  GITLAB_PAGE_SIZE = ENV['GITLAB_PAGE_SIZE'].to_i

  class << self
    attr_reader :images

    def setup
      @images = load_images
      @data = load_current_user_data
      @issues_data = load_issues_data
    rescue => error
      @errors = error
    end

    def render(menu_items, top_level: true)
      separator = top_level ? "\n---\n" : "\n--"
      output = top_level ? '' : separator
      output << menu_items.map(&:render).join(separator)
    end

    def username
      @data&.dig('data', 'currentUser', 'username') || errors_data
    end

    def menubar
      errors_data.merge(assigned_count: assignee['count'], request_count: reviewer['count'])
    end

    def assignee
      @data&.dig('data', 'currentUser', 'assignee') || errors_data
    end

    def reviewer
      @data&.dig('data', 'currentUser', 'reviewer') || errors_data
    end

    def todos
      @data&.dig('data', 'currentUser', 'todos') || errors_data
    end

    def issues
      @issues_data&.dig('data', 'issues') || errors_data
    end

    def version
      @data&.dig('data', 'metadata') || errors_data
    end

    private

    def load_images
      image_data = Utils.read_file('assets/images.json')
      JSON.parse(image_data)
    end

    def load_current_user_data
      response = ApiRequest.new(:current_user).execute
      parse_response(response)
    end

    def load_issues_data
      response = ApiRequest.new(:issues, { username: username }).execute
      parse_response(response)
    end

    def parse_response(response)
      parsed_response = JSON.parse(response)

      if errors = parsed_response['errors']
        message = errors.is_a?(Array) ? errors[0]['message'] : errors['message']
        raise StandardError.new(message)
      end

      parsed_response
    end

    def errors_data
      @errors ? { errors: @errors } : {}
    end
  end
end
